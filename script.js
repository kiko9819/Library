let myLibrary=[];
//constructor
class Book{
  constructor(author,title,pages,read){
    this.author=author;
    this.title=title;
    this.pages=pages;
    this.read=read;
  }
  info(){
    return this.author+", "+this.title+", "+this.pages+", "+this.read;
  }
  toggleRead(){
    this.read=!this.read;
  }
  _renderLibrary(){
    const bookList = document.querySelector("#books-list");
    bookList.innerHTML = "";
    const books = myLibrary.map((book, index) => this._createBookRow(book, index));
    books.forEach(book => bookList.appendChild(book));
  }
  _createBookRow(book,index){
    const card=document.createElement('div');
    card.className+="bookInfoContainer";
    card.dataset.index=index;
    card.textContent=book.info();

    const deleteBtn=document.createElement('button');
    deleteBtn.textContent="Delete";
    deleteBtn.className+="delete";
    deleteBtn.onclick=()=>{
      this.removeBookFromLibrary(index);
      this._renderLibrary();
    };

    const readBtn=document.createElement('button');
    readBtn.textContent="Read";
    readBtn.className+="read";
    readBtn.onclick=()=>{
      this.toggleRead();
      this._renderLibrary();
    };
    card.appendChild(deleteBtn);
    card.appendChild(readBtn);
    return card;
  }
  removeBookFromLibrary(index){
    myLibrary.splice(index,1);
  }
  addBookToLibrary(book){
    myLibrary.push(book);
  }
}

function hideModal(){
  document.getElementById('simpleModal').style.display="none";
}
function displayModal(){
  formHandler();
  document.getElementById('simpleModal').style.display="block";
}
function formHandler(){
  const sendBtn=document.getElementById('submitBtn');
  sendBtn.addEventListener('click',validationCheck);
}
function validationCheck(){
  const author=document.getElementById('author');
  const title=document.getElementById('title');
  const pages=document.getElementById('pages');
  const read=document.getElementById('checkBox');
  if(author.value==""||title.value==""||pages.value==""){
    alert("Need some input");
  } else {
    let newBook=new Book(author.value,title.value,pages.value,read.checked)
    newBook.addBookToLibrary(newBook);
    newBook._renderLibrary();
    hideModal();
  }
}
